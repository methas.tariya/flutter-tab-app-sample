import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sample_flutter_tab_app/collection_grid_tab.dart';
import 'package:sample_flutter_tab_app/collection_list_tab.dart';
import 'package:sample_flutter_tab_app/collection_mix_tab.dart';

class SampleFlutterTabApp extends StatelessWidget {
  const SampleFlutterTabApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CupertinoApp(
      theme: CupertinoThemeData(brightness: Brightness.light),
      home: SampleFlutterTabHome(),
    );
  }
}

// home page, which contains tab bar
class SampleFlutterTabHome extends StatelessWidget {
  const SampleFlutterTabHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.grid), label: 'First'),
            BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.list_bullet), label: 'Second'),
            BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.list_bullet_below_rectangle),
                label: 'Third'),
          ],
          backgroundColor: Colors.white70.withOpacity(1),
        ),
        tabBuilder: _appTabBuilder);
  }

  Widget _appTabBuilder(context, index) {
    late final CupertinoTabView tab;
    switch (index) {
      case 0:
        tab = CupertinoTabView(builder: (context) {
          return const CollectionGridPage();
        });
        break;
      case 1:
        tab = CupertinoTabView(builder: (context) {
          return const CollectionListPage();
        });
        break;
      case 2:
        tab = CupertinoTabView(builder: (context) {
          return const CollectionMixPage();
        });
        break;
    }
    return tab;
  }
}
