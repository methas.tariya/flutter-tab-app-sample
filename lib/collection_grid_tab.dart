import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CollectionGridPage extends StatelessWidget {
  const CollectionGridPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: const Text('First Tab - Grid'),
        backgroundColor: Colors.white70.withOpacity(1),
      ),
      child: GridView.count(
        crossAxisCount: 3,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        padding: const EdgeInsets.all(10),
        children: _getListData(context),
      ),
    );
  }

  _getListData(BuildContext context) {
    List<Widget> widgets = [];
    List<Color> colorList = [Colors.red, Colors.green, Colors.blue];
    for (int i = 0; i < 30; i++) {
      double cellWidth = MediaQuery.of(context).size.width / 3;
      widgets.add(GestureDetector(
        child: Center(
          child: Container(
            width: cellWidth,
            height: cellWidth,
            padding: const EdgeInsets.all(10),
            child: Container(
              child: Text('Box $i'),
              alignment: Alignment.center,
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                  color: colorList[i % 3],
                  borderRadius: const BorderRadius.all(Radius.circular(10))),
            ),
          ),
        ),
        onTap: () {
          showCupertinoModalPopup(
              context: context,
              builder: (context) => CupertinoActionSheet(
                    message: Center(child: Text('Box $i')),
                  ));
        },
      ));
    }

    return widgets;
  }
}
