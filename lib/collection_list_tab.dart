import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sample_flutter_tab_app/res.dart';

class CollectionListPage extends StatelessWidget {
  const CollectionListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: const Text('Second Tab - List'),
        trailing: CupertinoButton(
          child: Icon(CupertinoIcons.square_list),
          padding: EdgeInsets.zero,
          onPressed: () {
            print('nav button tapped');
          },
        ),
      ),
      child: ListView(children: _getListData(context)),
    );
  }

  _getListData(BuildContext context) {
    List<Widget> widgets = [];
    List<Color> colorList = [Colors.red, Colors.green, Colors.blue];
    for (int i = 0; i < 10; i++) {
      double cellWidth = MediaQuery.of(context).size.width / 3;
      widgets.add(GestureDetector(
        child: Center(
          child: Container(
            height: cellWidth,
            padding: const EdgeInsets.all(10),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Image.asset(Res.pepeThink_small),
                  ),
                  Text('Box $i')
                ],
              ),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: colorList[i % 3],
                  borderRadius: const BorderRadius.all(Radius.circular(10))),
            ),
          ),
        ),
        onTap: () {
          showCupertinoModalPopup(
              context: context,
              builder: (context) => CupertinoActionSheet(
                    message: Center(child: Text('Box $i')),
                  ));
        },
      ));
    }

    return widgets;
  }
}
