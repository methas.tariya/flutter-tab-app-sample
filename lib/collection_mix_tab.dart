import 'package:flutter/cupertino.dart';

class CollectionMixPage extends StatelessWidget {
  const CollectionMixPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Third Tab - Mixed List'),
      ),
      child: CustomScrollView(),
    );
  }
}
